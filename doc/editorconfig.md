# EditorConfig plugin for Lite

## How to use it
Just download editorconfig.lua and move it to the data/plugins
directory, which is located in the directory of your Lite executable.

Then you should create an .editorconfig file, either in your home
directory (`~/.editorconfig`) or in your project directory. The
plugin will try to find an .editorconfig file in parent directories,
your home directory or in the lite directory. 

For more information about creating .editorconfig files, visit
[http://editorconfig.org/](http://editorconfig.org/)

## Supported features
Currently my editorconfig plugin supports the following properties:
* indent_size
* indent_style
* line_height

I'll probably add support for more properties in the future, but for
now those are the only ones supported because Lite has a very limited
configuration file.

## Limitations
On Windows your .editorconfig file must be in the same directory as
lite.exe

