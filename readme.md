# Lite plugins
These are some plugins made by me for the
[lite text editor](https://github.com/rxi/lite), feel free to use them or
modify them. I'll be adding new plugins if I write more of them and I'll
be fixing bugs too.

## Plugin list
* __EditorConfig__: Lite had no editorconfig plugin available, so I wrote my
own editorconfig plugin. It has some limitations, but it doesn't depend on
the editorconfig core or any third-party lua module other than the Lite's
lua modules. For now my editorconfig plugin works fine on Linux and has
a more limited functionality on Windows (the .editorconfig file must be in the
same directory as lite.exe if you're using that OS). Read doc/editorconfig.md
for more info.

* (Future plugin) __Markdown Viewer__: I'm planning to make a markdown viewer
for Lite. I know that a markdown preview plugin
[already exists](https://raw.githubusercontent.com/rxi/lite-plugins/master/plugins/ghmarkdown.lua)
, but I don't like that it makes a request to github every time you want to
view your markdown files and I also don't like that it opens the web browser
instead of just creating a new text view in Lite.
