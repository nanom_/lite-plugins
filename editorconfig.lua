local core = require "core"
local Doc = require "core.doc"
local DocView = require "core.docview"
local config = require "core.config"

--[[
EditorConfig for lite by nanom 
released under the BSD 0 license

The goal of this editorconfig plugin is not to have any dependency (not even
editorconfig core)
Note:
Currently this plugin has limited functionality on Windows (.editorconfig MUST
be in the same directory as lite). On Linux/Unix it works just fine.
--]]

local function file_exists(filename)
  local f = io.open(filename, "r")
  if f ~= nil then
    io.close(f)
    return true
  end
  return false
end

local function capture(cmd)
  local f = assert(io.popen(cmd, "r"))
  local output = assert(f:read("*a"))
  f:close()
  output = output:gsub("^%s+", "")
  output = output:gsub("%s+$", "")
  output = output:gsub("[\n\r]+", " ")
  return output
end

local function replace_char_at(string, pos, replacement)
  assert(replacement:len() <= 1, "Invalid argument for 'replacement'!")
  return string:sub(1, pos - 1) .. replacement .. string:sub(pos + 1, string:len())
end

local function get_editorconfig_path(filename)
  assert(filename, "Error! Empty filename")
  if PLATFORM ~= "Windows" then
    local filepath = capture("readlink -f " .. filename)
    local dir = filepath:match("^(.*)[/\\].*$")
    local _, depth = dir:gsub("/", "")
    for i = 0, depth do
      if file_exists(dir .. "/.editorconfig") then
        return dir .. "/.editorconfig"
      end
      if dir == os.getenv("HOME") or dir == "/" then break end
      dir = dir:match("^(.*)[/\\].*$")
    end
    if file_exists(EXEDIR .. "/.editorconfig") then
      return EXEDIR .. "/.editorconfig"
    else
      return nil
    end
  else
    if file_exists(EXEDIR .. "\\.editorconfig") then
      return EXEDIR .. "\\.editorconfig"
    else
      return nil
    end
  end
end

-- adapted from LIP.lua by Dynodzzo
local function parse_editorconfig(filename)
  local f = assert(io.open(filename, "r"), "Couldn't open .editorconfig")
  local data = {}
  local section = ""
  for line in f:lines() do
    local tmp_section = line:match("^%[([^%[%]]+)%]$")
    if tmp_section then
      section = tmp_section
      data[section] = data[section] or {}
      goto continue
    end
    local key, val = line:match("^([%w|_]+)%s-=%s-(.+)$")
    if key and val ~= nil then
      if tonumber(val) then
        val = tonumber(val)
      elseif val == "true" then
        val = true
      elseif val == "false" then
        val = false
      end
      data[section] = data[section] or {}
      data[section][key] = val
    end
    ::continue::
  end
  f:close()
  return data
end

local function match_filename(filename, config)
  assert(type(config) == "table", "Invalid configuration!")
  assert(filename ~= "", "Error! empty filename")
  assert(filename:find("#") == nil and filename:find("&") == nil,
    "Please don't use '#' or '&' characters in your filenames!")
  local matches = {}
  local tmp_filename = filename:gsub("%.", "`")
  if tmp_filename:find("/") or tmp_filename:find("\\") then
    tmp_filename = tmp_filename:match("[/\\][^/\\]*$")
  end
  if tmp_filename:sub(1, 1) == "/" or tmp_filename:sub(1, 1) == "\\" then
    tmp_filename = replace_char_at(tmp_filename, 1, "")
  end
  for section in pairs(config) do
    if section == "*" or section == "" then
      goto continue
    end
    local lua_pattern = section
    local _, lists_and_ranges = lua_pattern:gsub("{[^{}]+}", "")
    local lists, ranges = {}, {}
    if lists_and_ranges > 0 then
      for range in lua_pattern:gmatch("{%-*%d+%.%.%-*%d+}") do
        local i = #ranges + 1
        ranges[i] = {}
        range = range:gsub("{", "")
        range = range:gsub("}", "")
        ranges[i][1] = tonumber(range:sub(1, range:find("%.") - 1))
        ranges[i][2] = tonumber(range:sub(range:find("%.%.") + 2, range:len()))
        assert(ranges[i][1] < ranges[i][2], "Invalid range in .editorconfig!")
      end
      lua_pattern = lua_pattern:gsub("{%-*%d+%.%.%-*%d+}", "#")
      lua_pattern = lua_pattern:gsub("%.", "`")
      lua_pattern = lua_pattern:gsub("%*%*", ".+")
      lua_pattern = lua_pattern:gsub("%*", ".+")
      lua_pattern = lua_pattern:gsub("?", ".?")
      for list in lua_pattern:gmatch("{[^{}]+}") do
        local i = #lists + 1
        local j = 1
        lists[i] = {}
        list = list:gsub("{", "")
        list = list:gsub("}", "")
        for word in list:gmatch("([^,]+)") do
          if word:sub(1, 1) == "" then
            word = replace_char_at(word, 1, "")
          end
          lists[i][j] = word
          j = j + 1
        end
      end
      lua_pattern = lua_pattern:gsub("{[^{}]+}", "&")
      -- compare pattern with the filename
      -- the following variables are substrings we need:
      local pattern_start = lua_pattern
      local filename_start, filename_end = "", tmp_filename
      local current_list, current_range, current_word = 1, 1, 1
      local match_ok = false
      for i = 1, lua_pattern:len() do
        if lua_pattern:sub(i, i) == "#" then
          pattern_start = pattern_start:sub(1, i - 1)
          for j = ranges[current_range][1], ranges[current_range][2] do
            local num_str = tostring(j)
            local _, num_str_count = tmp_filename:gsub(num_str, "")
            local tmp_pattern_start = pattern_start .. num_str
            for k = 0, num_str_count do
              local pos = tmp_filename:find(num_str)
              if pos == nil then break end
              filename_end = filename_end:sub(
                pos + num_str:len(),
                tmp_filename:len()
              )
              filename_start = tmp_filename:sub(1, pos + num_str:len() - 1)
              if filename_start:match(tmp_pattern_start) == filename_start then
                match_ok = true
                pattern_start = tmp_pattern_start
              end
            end 
          end
          if not match_ok then
            goto continue
          else
            current_range = current_range + 1
            match_ok = false
          end
        elseif lua_pattern:sub(i, i) == "&" then
          pattern_start = pattern_start:sub(1, i - 1)
          for j = 1, #lists[current_list] do
            local word = lists[current_list][j]
            local _, word_count = tmp_filename:gsub(word, "")
            local tmp_pattern_start = pattern_start .. word
            for k = 0, word_count do
              local pos = tmp_filename:find(word)
              if pos == nil then break end
              filename_end = filename_end:sub(
                pos + word:len(),
                tmp_filename:len()
              )
              filename_start = tmp_filename:sub(1, pos + word:len())
              if filename_start:match(tmp_pattern_start) == filename_start then
                match_ok = true
                pattern_start = tmp_pattern_start
              end
            end
          end
          if not match_ok then
            goto continue
          else
            current_list = current_list + 1
            match_ok = false
          end
        else
          pattern_start = pattern_start .. lua_pattern:sub(i, i)
        end
      end
      matches[#matches + 1] = section
    else
      lua_pattern = lua_pattern:gsub("%.", "`")
      lua_pattern = lua_pattern:gsub("%*%*", ".+")
      lua_pattern = lua_pattern:gsub("%*", ".+")
      lua_pattern = lua_pattern:gsub("?", ".?")
      if tmp_filename:match(lua_pattern) == tmp_filename then
        matches[#matches + 1] = section
      end
    end
    ::continue::
  end
  return matches
end

local function set_config(econfig, matching_sections)
  assert(type(econfig) == "table", "Invalid configuration!")
  assert(type(matching_sections) == "table", "Invalid sections argument!")
  -- for now these are the only three settings supported by the lite editor
  local indent_size = nil
  local indent_style = nil
  local line_height = nil
  -- configuration set by the [*] section
  local default_indent_size = nil
  local default_indent_style = nil
  local default_line_height = nil

  for section, cfg in pairs(econfig) do
    local section_ok = false
    for i = 1, #matching_sections do
      if matching_sections[i] == section then
        section_ok = true
      end
    end
    if not section_ok and section ~= "*" then goto continue end
    for key, value in pairs(cfg) do
      if key == "indent_style" then
        if value:sub(1, 1) == " " then
          value = replace_char_at(value, 1, "")
        end
        if section == "*" then
          default_indent_style = value
        else
          indent_style = value
        end
      elseif key == "indent_size" then
        if section == "*" then
          default_indent_size = tonumber(value) or nil
        else
          indent_size = tonumber(value) or nil
        end
      elseif key == "line_height" then
        if section == "*" then
          default_line_height = tonumber(value) or nil
        else
          line_height = tonumber(value) or nil
        end
      end
    end
    ::continue::
  end

  if not indent_style then
    if default_indent_style then
      if default_indent_style == "space" then
        config.tab_type = "soft"
      elseif default_indent_style == "tab" then
        config.tab_type = "hard"
      end
    end
  else
    if indent_style == "space" then
      config.tab_type = "soft"
    elseif indent_style == "tab" then
      config.tab_type = "hard"
    end
  end

  if indent_size then
    config.indent_size = indent_size
  elseif not indent_size and default_indent_size then
    config.indent_size = default_indent_size
  end

  if line_height then
    config.line_height = line_height
  elseif not line_height and default_line_height then
    config.line_height = default_line_height
  end
end

local econfig = {}
local matching_sections = {}
local node = core.root_view:get_active_node()
local node_id = node:get_view_idx(core.active_view)

local function editorconfig(filename)
  local editorconfig_path = get_editorconfig_path(filename)
  econfig = parse_editorconfig(editorconfig_path)
  matching_sections = match_filename(filename, econfig)
end

local new = Doc.new
function Doc:new(...)
  new(self, ...)
  local args = {...}
  local filename = args[1] or nil
  if filename then editorconfig(filename) end
end

local save = Doc.save
function Doc:save(...)
  save(self, ...)
  local args = {...}
  local filename = args[1] or nil
  if filename then editorconfig(filename) end
end

local draw = DocView.draw
function DocView:draw(...)
  draw(self, ...)
  set_config(econfig, matching_sections)
  node = core.root_view:get_active_node()
  if node_id ~= node:get_view_idx(core.active_view) then
    local filename = self.doc.filename
    if filename then editorconfig(filename) end
  end
  node_id = node:get_view_idx(core.active_view)
end

